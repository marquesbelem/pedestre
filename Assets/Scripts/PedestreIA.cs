﻿using UnityEngine;
using UnityEngine.AI;

public class PedestreIA : MonoBehaviour {

    [SerializeField]
    Transform target;
    NavMeshAgent pedestre;

    Vector3 destino;

    void Start () {
        pedestre = GetComponent<NavMeshAgent>();
        destino = pedestre.destination;
    }
	
	void Update () {
        pedestre.SetDestination(target.position);
    }
}
